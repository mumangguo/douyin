package com.douyin.vo;

import com.douyin.entity.Comments;
import com.douyin.entity.Likes;

import java.util.Date;
import java.util.List;

public class VideosVo {
    /**
     * 自增主键，视频唯一id
     */
    private Long id;
    /**
     * 视频作者id
     */
    private Long authorId;
    /**
     * 播放url
     */
    private String playUrl;
    /**
     * 封面url
     */
    private String coverUrl;
    /**
     * 发布时间戳
     */
    private Date publishTime;
    /**
     * 视频名称
     */
    private String title;

    private List<Likes> likes;
    private List<Comments> comments;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getPlayUrl() {
        return playUrl;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Likes> getLikes() {
        return likes;
    }

    public void setLikes(List<Likes> likes) {
        this.likes = likes;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }
}
