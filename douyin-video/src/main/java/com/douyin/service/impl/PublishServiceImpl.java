package com.douyin.service.impl;

import com.douyin.dto.AuthorDTO;
import com.douyin.dto.FeedDTO;
import com.douyin.dto.R;
import com.douyin.dto.VideoDTO;
import com.douyin.entity.Users;
import com.douyin.entity.Videos;
import com.douyin.mapper.CommentsMapper;
import com.douyin.mapper.LikesMapper;
import com.douyin.mapper.UsersMapper;
import com.douyin.mapper.VideosMapper;
import com.douyin.service.PublishService;
import com.douyin.util.UploadUtil;
import com.douyin.utils.JwtUtils;
import com.douyin.vo.VideosVo;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;
import java.util.concurrent.ThreadPoolExecutor;

@Service
public class PublishServiceImpl implements PublishService {
    public static final String ALI_DOMAIN = "https://douyin54007.oss-cn-beijing.aliyuncs.com/";

    @Resource
    private VideosMapper videosMapper;

    @Resource
    private UsersMapper usersMapper;
    @Resource
    private LikesMapper likesMapper;

    @Resource
    private CommentsMapper commentsMapper;

    @Resource
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 用户视频列表
     * @param token
     * @param user_id
     * @return
     */
    @Override
    public FeedDTO publishList(String token, String user_id) {
        /**
         *
         */
        List<VideoDTO> list=new ArrayList<>();
        //1.验证token是否对应
        String uid = JwtUtils.getMemberIdByJwtToken(token);
        if (uid==null || !user_id.equals(uid)) return new FeedDTO(1, "error", null, null);
        //2.查询user表构造 AuthorDTO
        Users users = usersMapper.selectUser(Integer.parseInt(user_id));
        AuthorDTO authorDTO = new AuthorDTO(users.getId(), users.getName(), users.getFollowCount(), users.getFollowerCount(), users.getIsFollow());
        //3.根据user_id查到对应的视频集合
        //联合查询 一个视频内对应多个点赞 多个评论
        List<VideosVo> videos = videosMapper.selectPublishList(Integer.parseInt(user_id));
        for(VideosVo v: videos){
            VideoDTO videoDTO = new VideoDTO(Integer.parseInt(v.getId()+""), authorDTO, v.getPlayUrl(), v.getCoverUrl(), v.getLikes().size(), v.getComments().size(), users.getIsFollow(), v.getTitle());
            list.add(videoDTO);
        }
        //4.构造结果
        return new FeedDTO(0, "success", null, list);
    }

    @Override
    public R action(MultipartFile data, String token, String title) {
        //file校验
        if(data.isEmpty()) {
            R r = new R();
            r.setStatus_code(1);
            r.setStatus_msg("data为null");
            return r;
        }

        String originalFilename = data.getOriginalFilename();
        String fileName = UUID.randomUUID().toString().replace("-", "") +"."+ FilenameUtils.getExtension(originalFilename);
        String url=ALI_DOMAIN+fileName;
        //线程池异步上传视频到oss
        FutureTask<String> futureTask = new FutureTask<>(new Callable<String>() {
            @Override
            public String call() throws Exception {
                //异步上传
                UploadUtil.upload(data,fileName);
                return null;
            }
        });
        threadPoolExecutor.submit(futureTask);
        //从100ms处截取,输出格式为jpg,宽度和高度随视频自适应,默认截图,自动旋转图片
        String coverUrl=url+"?x-oss-process=video/snapshot,t_100,f_jpg,w_0,h_0,m_fast,ar_auto";
        //存入数据库中
        Videos videos = new Videos();
        videos.setAuthorId(Long.parseLong(JwtUtils.getMemberIdByJwtToken(token)));
        videos.setPlayUrl(url);//视频地址
        videos.setCoverUrl(coverUrl);//视频封面
        videos.setPublishTime(new Date());
        videos.setTitle(title);
        int i = videosMapper.insert(videos);
        if(i!=1) {
            R r = new R();
            r.setStatus_code(1);
            r.setStatus_msg("error");
            return r;
        }
        R r = new R();
        r.setStatus_code(0);
        r.setStatus_msg("success");
        return r;
    }


}
