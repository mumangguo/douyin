package com.douyin.service.impl;

import com.douyin.dto.AuthorDTO;
import com.douyin.dto.FeedDTO;
import com.douyin.dto.VideoDTO;
import com.douyin.entity.Comments;
import com.douyin.entity.Likes;
import com.douyin.entity.Users;
import com.douyin.entity.Videos;
import com.douyin.mapper.CommentsMapper;
import com.douyin.mapper.LikesMapper;
import com.douyin.mapper.UsersMapper;
import com.douyin.mapper.VideosMapper;
import com.douyin.service.FeedService;
import com.douyin.utils.JwtUtils;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class FeedServiceImpl implements FeedService {
    @Resource
    private VideosMapper videosMapper;

    @Resource
    private UsersMapper usersMapper;
    @Resource
    private LikesMapper likesMapper;

    @Resource
    private CommentsMapper commentsMapper;

    @Override
    public FeedDTO feed(String latest_time, String token) {
        //时间倒序 返回最多30条视频数据 也就是最新的视频在前面
        //返回latest_time时间戳之前的视频，没有就默认当前时间之前的视频
        List<Videos> videos=null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if("0".equals(latest_time)){
            System.out.println("查找最新视频");
            videos = videosMapper.selectLimitCount(simpleDateFormat.format(new Date()));
        }else{
            //latest_time时间戳之前的视频
            System.out.println("查找latest_time时间戳之前的视频");
            videos = videosMapper.selectLimitCount(simpleDateFormat.format(new Date(Long.parseLong(latest_time))));
        }
        //抽出发布最早视频的时间
        if (videos==null || videos.size()==0) return new FeedDTO(1, "正在加载", null, null);

        Long publishTime = videos.get(videos.size() - 1).getPublishTime().getTime() ;
        List<VideoDTO> list = new ArrayList<>();
        for(Videos v:videos){
            //查到关联作者信息
            Users users = usersMapper.selectUser(Integer.parseInt(v.getAuthorId() + ""));
            AuthorDTO authorDTO = new AuthorDTO(Integer.parseInt(users.getId() + ""), users.getName(), users.getFollowCount(), users.getFollowerCount(), users.getIsFollow());
            //统计当前视频 点赞总数 和 评论总数
            Example likesExample = new Example(Likes.class);
            likesExample.createCriteria().andEqualTo("videoId", v.getId()).andEqualTo("cancel", 0);
            int likesCount = likesMapper.selectCountByExample(likesExample);

            Example commentsExample = new Example(Comments.class);
            likesExample.createCriteria().andEqualTo("videoId", v.getId()).andEqualTo("cancel", 0);
            int commentsCount = commentsMapper.selectCountByExample(commentsExample);
            //
            // 如果token不为空 查询该用户对此视频是否已点赞
            boolean favorite=false;
            if (token!=null){
                String id = JwtUtils.getMemberIdByJwtToken(token);
                //查询当前用户是否对该视频点赞
                Example example = new Example(Likes.class);
                example.createCriteria().andEqualTo("userId", id).andEqualTo("videoId", v.getId());
                Likes likes = likesMapper.selectOneByExample(example);
                if (likes!=null && likes.getCancel() == 0) {
                    favorite = true;
                }
            }
            VideoDTO videoDTO = new VideoDTO(Integer.parseInt(v.getId() + ""), authorDTO, v.getPlayUrl(), v.getCoverUrl(), likesCount, commentsCount, favorite, v.getTitle());
            list.add(videoDTO);

        }
        return new FeedDTO(0,"success",publishTime,list);
    }
}
