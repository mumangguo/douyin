package com.douyin.service;

import com.douyin.dto.FeedDTO;
import com.douyin.dto.R;
import org.springframework.web.multipart.MultipartFile;

public interface PublishService {

    R action(MultipartFile data, String token, String title);

    FeedDTO publishList(String token, String user_id);
}
