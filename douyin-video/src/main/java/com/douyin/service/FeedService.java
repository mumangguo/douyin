package com.douyin.service;

import com.douyin.dto.FeedDTO;

public interface FeedService {
    public FeedDTO feed(String latest_time,String token);
}
