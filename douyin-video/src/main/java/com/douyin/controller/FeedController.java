package com.douyin.controller;

import com.douyin.dto.FeedDTO;
import com.douyin.service.FeedService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping("/feed")
public class FeedController {
    @Resource
    private FeedService feedService;

    @GetMapping("")
    public FeedDTO feed(@RequestParam(value = "latest_time",required = false) String latest_time,
                        @RequestParam(value = "token",required = false) String token){

        System.out.println("进入视频流");
        return feedService.feed(latest_time, token);
    }

}
