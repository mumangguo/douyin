package com.douyin.controller;

import com.douyin.dto.FeedDTO;
import com.douyin.dto.R;
import com.douyin.service.PublishService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

@RestController
@CrossOrigin
@RequestMapping("/publish")
public class PublishController {
    @Resource
    private PublishService publishService;

    @GetMapping("/list/")
    public FeedDTO publishList(@RequestParam("token") String token,
                               @RequestParam("user_id") String user_id) {

        System.out.println("进入用户视频列表");
        return publishService.publishList(token, user_id);
    }

    @PostMapping("/action/")
    public R action(@RequestPart(value = "data", required = true) MultipartFile data,
                    @RequestParam(value = "token", required = true) String token,
                    @RequestParam(value = "title", required = true) String title) {

        System.out.println(data);
        return publishService.action(data, token, title);
    }
}
