package com.douyin.mapper;

import com.douyin.entity.Likes;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import javax.annotation.Resource;

@Resource
public interface LikesMapper extends Mapper<Likes>, MySqlMapper<Likes> {
}
