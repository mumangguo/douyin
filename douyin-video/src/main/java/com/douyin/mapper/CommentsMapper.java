package com.douyin.mapper;

import com.douyin.entity.Comments;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface CommentsMapper extends Mapper<Comments>, MySqlMapper<Comments> {
}
