package com.douyin.mapper;

import com.douyin.entity.Videos;
import com.douyin.vo.VideosVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.Date;
import java.util.List;

@Repository
public interface VideosMapper extends Mapper<Videos>, MySqlMapper<Videos> {
    List<Videos> selectLimitCount(@Param("time")String time);

    List<VideosVo> selectPublishList(@Param("uid")Integer uid);
}
