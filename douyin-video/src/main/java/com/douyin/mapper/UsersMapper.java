package com.douyin.mapper;

import com.douyin.entity.Users;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

@Repository
public interface UsersMapper extends MySqlMapper<Users>, Mapper<Users> {
public Users selectUser(@Param("userId") Integer userId);
}
