package com.douyin.dto;

public class AuthorDTO {
    private Integer id;
    private String name;//姓名
    private Integer follow_count;//关注总数
    private Integer follower_count;//粉丝总数
    private Boolean is_follow;//true-已关注，false-未关注

    public AuthorDTO() {
    }

    public AuthorDTO(Integer id, String name, Integer follow_count, Integer follower_count, Boolean is_follow) {
        this.id = id;
        this.name = name;
        this.follow_count = follow_count;
        this.follower_count = follower_count;
        this.is_follow = is_follow;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getFollow_count() {
        return follow_count;
    }

    public void setFollow_count(Integer follow_count) {
        this.follow_count = follow_count;
    }

    public Integer getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(Integer follower_count) {
        this.follower_count = follower_count;
    }

    public Boolean getIs_follow() {
        return is_follow;
    }

    public void setIs_follow(Boolean is_follow) {
        this.is_follow = is_follow;
    }
}
