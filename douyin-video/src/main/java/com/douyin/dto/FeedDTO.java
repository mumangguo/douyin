package com.douyin.dto;

import java.util.Date;
import java.util.List;

public class FeedDTO  {
    private Integer status_code; //状态码，0-成功，其他值-失败
    private String status_msg; //返回状态描述12
    private Long next_time;//本次返回的视频中，发布最早的时间，作为下次请求时的latest_time
    private List<VideoDTO> video_list;

    public FeedDTO() {
    }
    public FeedDTO(Integer status_code, String status_msg, Long next_time, List<VideoDTO> video_list) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.next_time = next_time;
        this.video_list = video_list;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public Long getNext_time() {
        return next_time;
    }

    public void setNext_time(Long next_time) {
        this.next_time = next_time;
    }

    public List<VideoDTO> getVideo_list() {
        return video_list;
    }

    public void setVideo_list(List<VideoDTO> video_list) {
        this.video_list = video_list;
    }
}
