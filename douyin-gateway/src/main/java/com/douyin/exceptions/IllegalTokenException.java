package com.douyin.exceptions;

/**
 * 非法TOKEN异常
 */
public class IllegalTokenException extends RuntimeException {
    public IllegalTokenException(String message) {
        super(message);
    }
}
