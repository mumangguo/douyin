package com.douyin.filter.factory;

import com.douyin.exceptions.IllegalTokenException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * 自定义全局网关过滤器
 */
@Configuration
public class GlobalGateFilter implements GlobalFilter, Ordered {
    private static final Logger logger = LoggerFactory.getLogger(GlobalGateFilter.class);

    //类似Javaweb doFilter
    //exchange：交换 request,response
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        logger.info("进入网关过滤器");
        //相当于HttpRequest对象
        ServerHttpRequest request = exchange.getRequest();
        //相当于HttpResponse对象
        ServerHttpResponse response = exchange.getResponse();
        logger.info("请求路径：{}", request.getURI().getPath());
        //如果是注册登录和视频流就放行
        if ("/douyin/feed/".equals(request.getURI().getPath()) || "/douyin/user/register/".equals(request.getURI().getPath()) || "/douyin/user/login/".equals(request.getURI().getPath())) {
            return chain.filter(exchange);
        }
        //获取请求参数,拿到token
        if (request.getQueryParams().get("token") == null) throw new IllegalTokenException("未经授权访问!");
        String token = request.getQueryParams().get("token").get(0);
        logger.info("token:{}", token);
        //校验token 后续完成

        //放行
        Mono<Void> filter = chain.filter(exchange);
        return filter;
    }

    //order排序  int数字：用来指定filter执行顺序 默认按照自然数数字顺序执行
    @Override
    public int getOrder() {
        return -1;
    }
}
