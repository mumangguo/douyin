package com.douyin.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douyin.entity.Comments;
import com.douyin.mapper.CommentMapper;
import com.douyin.mapper.UserMapper;
import com.douyin.service.CommentService;
import com.douyin.utils.JwtUtils;
import com.douyin.vo.Comment;
import com.douyin.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Service
public class CommentServiceImpl extends ServiceImpl<CommentMapper, Comments> implements CommentService {

    @Autowired
    private CommentMapper commentMapper;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<Comment> addCommentAction(String token, String videoId, String commentText) {

        Comments comments = new Comments();
        List<Comment> commentList = new ArrayList<>();
        Date date = new Date();
        String userId = JwtUtils.getMemberIdByJwtToken(token);
        comments.setUserId(Long.valueOf(userId));
        comments.setVideoId(Long.valueOf(videoId));
        comments.setCommentText(commentText);
        comments.setCreateDate(date);
        commentMapper.insert(comments);

        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id", userId).eq("video_id", videoId).eq("comment_text", commentText);
        Comments commentId = getOne(wrapper);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM-hh");
        String time = dateFormat.format(commentId.getCreateDate());
        UserVo userVo = userMapper.queryUserById(userId);
        Comment comment = new Comment();
        comment.setId(commentId.getId());
        comment.setUser(userVo);
        comment.setContent(commentText);
        comment.setCreate_date(time);
        commentList.add(comment);


        return commentList;
    }

    @Override
    public void dlecommentAction(String commentId) {
        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        wrapper.eq("id", commentId);
        commentMapper.delete(wrapper);
    }

    /**
     * 获取评论列表
     *
     * @param videoId
     * @return
     */
    @Override
    public List<Comment> commentList(String videoId) {
        QueryWrapper<Comments> wrapper = new QueryWrapper<>();
        List<Comment> commentList = new ArrayList<>();
        // 根据视频id查询用户id列表
        wrapper.eq("video_id", videoId);
        List<Comments> commentsList = list(wrapper);
        for (Comments comments : commentsList) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-hh");
            String time = dateFormat.format(comments.getCreateDate());
            //根据用户列表的用户id查询用户信息
            UserVo userVo = userMapper.queryUserById(String.valueOf(comments.getUserId()));
            //拼装数据
            Comment comment = new Comment();
            comment.setId(comments.getId());
            comment.setUser(userVo);
            comment.setContent(comments.getCommentText());
            comment.setCreate_date(time);
            if (userVo != null) {
                commentList.add(comment);
            }
        }
        return commentList;
    }


}
