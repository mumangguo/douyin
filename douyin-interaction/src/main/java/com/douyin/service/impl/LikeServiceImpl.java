package com.douyin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douyin.dto.LikeDto;
import com.douyin.dto.LikeListDto;
import com.douyin.dto.R;
import com.douyin.entity.Likes;
import com.douyin.mapper.LikeMapper;
import com.douyin.mapper.UserMapper;
import com.douyin.service.LikeService;
import com.douyin.utils.JwtUtils;
import com.douyin.vo.VideoUserVo;
import com.douyin.vo.VideoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LikeServiceImpl extends ServiceImpl<LikeMapper, Likes> implements LikeService {
    @Autowired
    LikeMapper likeMapper;

    @Autowired
    UserMapper userMapper;


    @Override
    public R likeAction(String token, String videoId, String actionType) {
        String userId = JwtUtils.getMemberIdByJwtToken(token);
        Boolean isTrue = likeMapper.likeAction(userId, videoId, actionType);
        if (isTrue) {
            return new R(0, "success");
        }
        return new R(1, "fail");
    }

    @Override
    public LikeDto likeList(String userId, String token) {
        String authorId = JwtUtils.getMemberIdByJwtToken(token);
        List<Likes> likes = likeMapper.selectList(new LambdaQueryWrapper<Likes>().eq(Likes::getUserId, userId).eq(Likes::getCancel, 0));
        LikeDto likeDto = new LikeDto(0, "success", new ArrayList<>());
        for (int i = 0; i < likes.size(); i++) {
            LikeListDto likeListDto = new LikeListDto();
            VideoUserVo userVo = userMapper.queryVideoUserById(String.valueOf(likes.get(i).getUserId()));
            VideoVo videoVo = likeMapper.queryVideoById(likes.get(i).getVideoId(), authorId);
            BeanUtils.copyProperties(videoVo, likeListDto);
            likeListDto.setAuthor(userVo);
            likeDto.getVideo_list().add(likeListDto);
        }
        return likeDto;
    }
}

