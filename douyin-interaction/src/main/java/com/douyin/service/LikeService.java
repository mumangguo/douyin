package com.douyin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douyin.dto.LikeDto;
import com.douyin.dto.LikeListDto;
import com.douyin.dto.R;
import com.douyin.entity.Comments;
import com.douyin.entity.Likes;
import com.douyin.vo.Comment;

import java.util.List;

public interface LikeService extends IService<Likes> {
    R likeAction(String token, String videoId, String actionType);

    LikeDto likeList(String userId,String token);
}


