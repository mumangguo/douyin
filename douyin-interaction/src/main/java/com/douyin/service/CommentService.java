package com.douyin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douyin.entity.Comments;
import com.douyin.vo.Comment;

import java.util.List;

public interface CommentService extends IService<Comments> {

    List<Comment> addCommentAction(String token, String videoId, String commentText);

    void dlecommentAction(String commentId);

    List<Comment> commentList(String videoId);

}
