package com.douyin.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserVo extends Comment {

    /**
     * 用户id
     */
    private int id;
    /**
     * 用户名
     */
    private String name;

    /**
     * 关注数
     */
    private int follow_count;
    /**
     * 粉丝数
     */
    private int follower_count;
    /**
     * 是否已关注
     */
    private boolean is_follow;

    public UserVo() {
    }

    public UserVo(int id, String name, int followCount, int followerCount, boolean isFollow) {
        this.id = id;
        this.name = name;
        this.follow_count = followCount;
        this.follower_count = followerCount;
        this.is_follow = isFollow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("follow_count")
    public int getFollowCount() {
        return follow_count;
    }

    public void setFollowCount(int followCount) {
        this.follow_count = followCount;
    }

    @JsonProperty("follower_count")
    public int getFollowerCount() {
        return follower_count;
    }

    public void setFollowerCount(int followerCount) {
        this.follower_count = followerCount;
    }

    @JsonProperty("is_follow")
    public boolean isFollow() {
        return is_follow;
    }

    public void setFollow(boolean follow) {
        is_follow = follow;
    }
}
