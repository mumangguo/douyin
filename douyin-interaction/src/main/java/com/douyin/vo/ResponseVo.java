package com.douyin.vo;

public class ResponseVo<T> {
    /**
     * 状态码，0是成功 其他值是失败
     */
    private Integer status_code;
    /**
     * 状态描述
     */
    private String status_msg;
    /**
     * 返回的数据
     */
    private T data;

    public ResponseVo(Integer status_code) {
        this.status_code = status_code;
    }

    public ResponseVo(Integer status_code, String status_msg, T data) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.data = data;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
    public ResponseVo(Integer status_code, String status_msg) {
        this.status_code = status_code;
        this.status_msg = status_msg;
    }
    public static <T> ResponseVo<T> success(T data){
        ResponseVo<T> responseDTO = new ResponseVo<T>(0, "成功", data);
        return responseDTO;
    }
    public static ResponseVo fail(){
        ResponseVo responseDTO = new ResponseVo(-1, "失败");
        return responseDTO;
    }
    public static ResponseVo fail(String statusMsg){
        ResponseVo responseDTO = new ResponseVo(-1, statusMsg);
        return responseDTO;
    }
}
