package com.douyin.vo;

public class Comment {
    /**
     * 评论id
     */
    private Integer id;

    /**
     * User 评论用户信息
     */
    private UserVo user;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 评论发布日期,格式为mm--dd
     */
    private String create_date;

    public Comment() {
    }

    public Comment(Integer id, UserVo user, String content, String create_date) {
        this.id = id;
        this.user = user;
        this.content = content;
        this.create_date = create_date;
    }

    public int getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserVo getUser() {
        return user;
    }

    public void setUser(UserVo user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }
}
