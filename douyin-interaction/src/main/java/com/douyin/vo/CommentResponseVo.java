package com.douyin.vo;

import java.util.List;

public class CommentResponseVo {
    /**
     * 状态码，0-成功，其他值-失败
     */
    private Integer status_code;
    /**
     * 返回状态描述
     */
    private String status_msg;

    /**
     * 评论成功返回评论内容，不需要重新拉取整个列表
     */
    private List<Comment> comment_list;

    public CommentResponseVo() {
    }

    public CommentResponseVo(Integer status_code, String status_msg, List<Comment> comment_list) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.comment_list = comment_list;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public List<Comment> getComment_list() {
        return comment_list;
    }

    public void setComment_list(List<Comment> comment_list) {
        this.comment_list = comment_list;
    }
}

