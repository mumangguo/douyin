package com.douyin.vo;

public class VideoUserVo {
    /**
     * 用户id
     */
    private int id;
    /**
     * 用户名
     */
    private String name;

    /**
     * 关注数
     */
    private int follow_count;
    /**
     * 粉丝数
     */
    private int follower_count;
    /**
     * 是否已关注
     */
    private boolean is_follow;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getFollow_count() {
        return follow_count;
    }

    public void setFollow_count(int follow_count) {
        this.follow_count = follow_count;
    }

    public int getFollower_count() {
        return follower_count;
    }

    public void setFollower_count(int follower_count) {
        this.follower_count = follower_count;
    }

    public boolean isIs_follow() {
        return is_follow;
    }

    public void setIs_follow(boolean is_follow) {
        this.is_follow = is_follow;
    }

    public VideoUserVo(int id, String name, int follow_count, int follower_count, boolean is_follow) {
        this.id = id;
        this.name = name;
        this.follow_count = follow_count;
        this.follower_count = follower_count;
        this.is_follow = is_follow;
    }
}
