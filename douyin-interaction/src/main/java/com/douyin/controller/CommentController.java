package com.douyin.controller;

import com.douyin.mapper.UserMapper;
import com.douyin.service.CommentService;
import com.douyin.vo.CommentResponseVo;
import com.douyin.vo.Comment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/comment")
public class CommentController {

    private static final Logger logger = LoggerFactory.getLogger(CommentController.class);

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserMapper userMapper;

    @PostMapping("/action")
    public CommentResponseVo commentAction(@RequestParam("token") String token,
                                           @RequestParam("video_id") String videoId,
                                           @RequestParam("action_type") String actionType,
                                           @RequestParam(value = "comment_text",required = false) String commentText,
                                           @RequestParam(value = "comment_id",required = false) String commentId){
        CommentResponseVo commentResponseVo = new CommentResponseVo();

        if (actionType.equals("1")) {
            List<Comment> comment = commentService.addCommentAction(token,videoId,commentText);
            commentResponseVo.setStatus_code(0);
            commentResponseVo.setStatus_msg("成功");
            commentResponseVo.setComment_list(comment);
            return commentResponseVo;
        }
        if (actionType.equals("2")){
            commentService.dlecommentAction(commentId);
            commentResponseVo.setStatus_code(0);
            commentResponseVo.setStatus_msg("删除成功");
            return commentResponseVo;
        }
        return null;
    }

    @GetMapping("/list")
    public CommentResponseVo commentList(@RequestParam("token") String token,
                                         @RequestParam("video_id") String videoId){
        CommentResponseVo commentResponseVo = new CommentResponseVo();
        try {
            List<Comment> comments =  commentService.commentList(videoId);
            commentResponseVo.setStatus_code(0);
            commentResponseVo.setStatus_msg("ss");
            commentResponseVo.setComment_list(comments);
            return commentResponseVo;
        }catch (Exception e) {
            commentResponseVo.setStatus_code(1);
            commentResponseVo.setStatus_msg(e.getMessage());
            return commentResponseVo;
        }

    }
}
