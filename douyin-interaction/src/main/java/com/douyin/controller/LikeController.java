package com.douyin.controller;

import com.douyin.dto.LikeDto;
import com.douyin.dto.LikeListDto;
import com.douyin.dto.R;
import com.douyin.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/favorite")
public class LikeController {

    @Autowired
    private LikeService likeService;


    @PostMapping("action")
    public R likeAction(@RequestParam("token") String token,
                        @RequestParam("video_id") String videoId,
                        @RequestParam("action_type") String actionType){
      return  likeService.likeAction(token,videoId,actionType);

    }

    @GetMapping("list")
    public LikeDto likeAction(@RequestParam("user_id") String userId, @RequestParam("token") String token){
        return likeService.likeList(userId,token);

    }


}
