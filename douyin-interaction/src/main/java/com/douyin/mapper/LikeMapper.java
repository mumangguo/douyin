package com.douyin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douyin.entity.Likes;
import com.douyin.vo.VideoVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LikeMapper extends BaseMapper<Likes> {
    Boolean likeAction(@Param("userId") String userId, @Param("videoId")String videoId, @Param("actionType")String actionType);

    VideoVo queryVideoById(@Param("videoId") Long videoId,@Param("authorId") String authorId);
}
