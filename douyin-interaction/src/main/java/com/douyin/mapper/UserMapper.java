package com.douyin.mapper;

import com.douyin.vo.UserVo;
import com.douyin.vo.VideoUserVo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserMapper {

    /**
     * 根据用户id查询用户信息
     */
    UserVo queryUserById(String id);

    VideoUserVo queryVideoUserById(String id);
}
