package com.douyin.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douyin.entity.Comments;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommentMapper extends BaseMapper<Comments> {
}
