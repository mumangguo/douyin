package com.douyin.dto;

import java.util.List;

public class LikeDto{

    private static final long serialVersionUID = 1L;

    private Integer status_code; //状态码，0-成功，其他值-失败

    private String status_msg; //返回状态描述
    private List<LikeListDto> video_list;

    public LikeDto(Integer status_code, String status_msg, List<LikeListDto> video_list) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.video_list = video_list;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public List<LikeListDto> getVideo_list() {
        return video_list;
    }

    public void setVideo_list(List<LikeListDto> video_list) {
        this.video_list = video_list;
    }

    public LikeDto() {
    }
}
