package com.douyin.dto;

import com.douyin.vo.UserVo;
import com.douyin.vo.VideoUserVo;

public class LikeListDto {
    private Integer id;
    private VideoUserVo author;//User视频作者信息
    private String play_url;//视频播放地址
    private String cover_url;//视频封面地址
    private Integer favorite_count;//视频的点赞总数
    private Integer comment_count;//视频的评论总数
    private Boolean is_favorite;//true-已点赞，false-未点赞
    private String title;//视频标题

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public VideoUserVo getAuthor() {
        return author;
    }

    public void setAuthor(VideoUserVo author) {
        this.author = author;
    }

    public String getPlay_url() {
        return play_url;
    }

    public void setPlay_url(String play_url) {
        this.play_url = play_url;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public Integer getFavorite_count() {
        return favorite_count;
    }

    public void setFavorite_count(Integer favorite_count) {
        this.favorite_count = favorite_count;
    }

    public Integer getComment_count() {
        return comment_count;
    }

    public void setComment_count(Integer comment_count) {
        this.comment_count = comment_count;
    }

    public Boolean getIs_favorite() {
        return is_favorite;
    }

    public void setIs_favorite(Boolean is_favorite) {
        this.is_favorite = is_favorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LikeListDto(Integer id, VideoUserVo author, String play_url, String cover_url, Integer favorite_count, Integer comment_count, Boolean is_favorite, String title) {
        this.id = id;
        this.author = author;
        this.play_url = play_url;
        this.cover_url = cover_url;
        this.favorite_count = favorite_count;
        this.comment_count = comment_count;
        this.is_favorite = is_favorite;
        this.title = title;
    }

    public LikeListDto() {
    }
}
