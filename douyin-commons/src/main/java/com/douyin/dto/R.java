package com.douyin.dto;

import java.io.Serializable;

/**
 * 通用响应结果返回类，返回数据继承此类丰富对象即可
 */
public class R implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer status_code; //状态码，0-成功，其他值-失败

    private String status_msg; //返回状态描述

    public R() {

    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public R(Integer status_code, String status_msg) {
        this.status_code = status_code;
        this.status_msg = status_msg;
    }

    public static R success(String status_msg){
        return new R(0,status_msg);
    }

    public static R fail(String status_msg){
        return new R(-1,status_msg);
    }
}
