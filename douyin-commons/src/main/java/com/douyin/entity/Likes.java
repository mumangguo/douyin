package com.douyin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * 点赞表(Likes)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:29
 */
public class Likes implements Serializable {
    private static final long serialVersionUID = -36706501783959364L;
    /**
     * 自增主键
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Long id;
    /**
     * 点赞用户id
     */
    private Long userId;
    /**
     * 被点赞的视频id
     */
    private Long videoId;
    /**
     * 默认点赞为0，取消赞为1
     */
    private Integer cancel;

    public Likes() {
    }

    public Likes(Long id, Long userId, Long videoId, Integer cancel) {
        this.id = id;
        this.userId = userId;
        this.videoId = videoId;
        this.cancel = cancel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getVideoId() {
        return videoId;
    }

    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    public Integer getCancel() {
        return cancel;
    }

    public void setCancel(Integer cancel) {
        this.cancel = cancel;
    }

}

