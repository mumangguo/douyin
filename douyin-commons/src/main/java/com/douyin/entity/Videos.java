package com.douyin.entity;

import java.util.Date;
import java.io.Serializable;

/**
 *
视频表(Videos)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:29
 */
public class Videos implements Serializable {
    private static final long serialVersionUID = -19272503332290114L;
    /**
     * 自增主键，视频唯一id
     */
    private Long id;
    /**
     * 视频作者id
     */
    private Long authorId;
    /**
     * 播放url
     */
    private String playUrl;
    /**
     * 封面url
     */
    private String coverUrl;
    /**
     * 发布时间戳
     */
    private Date publishTime;
    /**
     * 视频名称
     */
    private String title;

    public Videos() {
    }

    public Videos(Long id, Long authorId, String playUrl, String coverUrl, Date publishTime, String title) {
        this.id = id;
        this.authorId = authorId;
        this.playUrl = playUrl;
        this.coverUrl = coverUrl;
        this.publishTime = publishTime;
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public String getPlayUrl() {
        return playUrl;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public Date getPublishTime() {
        return publishTime;
    }

    public void setPublishTime(Date publishTime) {
        this.publishTime = publishTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}

