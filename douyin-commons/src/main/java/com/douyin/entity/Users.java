package com.douyin.entity;

import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * 用户表(Users)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:29
 */
@TableName(value = "users")
public class Users implements Serializable {
    private static final long serialVersionUID = 569612068214892433L;
    /**
     * 用户id
     */
    private int id;
    /**
     * 用户名
     */
    private String name;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 关注数
     */
    private int followCount;
    /**
     * 粉丝数
     */
    private int followerCount;
    /**
     * 是否已关注
     */
    private boolean isFollow;

    public Users() {
    }

    public Users(int id, String name, String password, int followCount, int followerCount, boolean isFollow) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.followCount = followCount;
        this.followerCount = followerCount;
        this.isFollow = isFollow;
    }

    public Users(String id, Object o, int followCount, int followCount1, Object o1) {
    }



    public int getFollowCount() {
        return followCount;
    }

    public void setFollowCount(int followCount) {
        this.followCount = followCount;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public boolean getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(boolean isFollow) {
        this.isFollow = isFollow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}

