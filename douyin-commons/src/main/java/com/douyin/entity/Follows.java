package com.douyin.entity;

import com.baomidou.mybatisplus.annotation.TableField;

import java.io.Serializable;

/**
 * 关注表(Follows)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:29
 */
public class Follows implements Serializable {
    private static final long serialVersionUID = 928847718848523117L;
    /**
     * 自增主键
     */
    private Long id;
    /**
     * 用户id
     */
    @TableField("user_id")
    private Long user_id;
    /**
     * 关注的用户
     */
    @TableField("follower_id")
    private Long follower_id;
    /**
     * 默认关注为0，取消关注为1
     */
    private Integer cancel;

    public Follows() {
    }

    public Follows(Long user_id, Long follower_id, Integer cancel) {
        this.user_id = user_id;
        this.follower_id = follower_id;
        this.cancel = cancel;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getFollower_id() {
        return follower_id;
    }

    public void setFollower_id(Long follower_id) {
        this.follower_id = follower_id;
    }

    public Integer getCancel() {
        return cancel;
    }

    public void setCancel(Integer cancel) {
        this.cancel = cancel;
    }
}

