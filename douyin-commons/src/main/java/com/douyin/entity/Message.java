package com.douyin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * (Message)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:29
 */
public class Message implements Serializable {
    private static final long serialVersionUID = 943214603370168998L;
    /**
     * 消息id
     */
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    /**
     * 发送方ID
     */
    @TableField("user_id")
    private int userId;
    /**
     * 发送方ID
     */
    @TableField("to_user_id")
    private int toUserId;

    public Message(Integer id, int userId, int toUserId, String content, String createTime) {
        this.id = id;
        this.userId = userId;
        this.toUserId = toUserId;
        this.content = content;
        this.createTime = createTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getToUserId() {
        return toUserId;
    }

    public void setToUserId(int toUserId) {
        this.toUserId = toUserId;
    }

    /**
     * 消息内容
     */
    private String content;
    /**
     * 消息发送时间
     */
    @TableField("create_time")
    private String createTime;

    public Message() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

}

