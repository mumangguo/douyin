package com.douyin.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * 评论表(Comments)实体类
 *
 * @author makejava
 * @since 2023-01-13 17:25:28
 */
public class Comments implements Serializable {
    private static final long serialVersionUID = -58344794066085786L;
    /**
     * 评论id，自增主键
     */
    private int id;
    /**
     * 评论发布用户id
     */
    private Long userId;
    /**
     * 评论视频id
     */
    private Long videoId;
    /**
     * 评论内容
     */
    private String commentText;
    /**
     * 评论发布时间
     */
    private Date createDate;
    /**
     * 默认评论发布为0，取消后为1
     */
    private Integer cancel;

    public Comments() {
    }

    public Comments(int id, Long userId, Long videoId, String commentText, Date createDate, Integer cancel) {
        this.id = id;
        this.userId = userId;
        this.videoId = videoId;
        this.commentText = commentText;
        this.createDate = createDate;
        this.cancel = cancel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getVideoId() {
        return videoId;
    }

    public void setVideoId(Long videoId) {
        this.videoId = videoId;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Integer getCancel() {
        return cancel;
    }

    public void setCancel(Integer cancel) {
        this.cancel = cancel;
    }

}

