package com.douyin.utils;


import com.douyin.entity.Users;

/**
 * 获取用户信息
 */
public class UserHolder {
    private static final ThreadLocal<Users> tl = new ThreadLocal<>();

    public static void saveUser(Users user){
        tl.set(user);
    }

    public static Users getUser(){
        return tl.get();
    }

    public static void removeUser(){
        tl.remove();
    }
}
