package com.douyin.dto;

public class LoginDTO{
    private Integer status_code; //状态码，0-成功，其他值-失败

    private String status_msg; //返回状态描述12
    private Integer user_id;
    private String token;

    public LoginDTO(Integer status_code, String status_msg, Integer user_id, String token) {
        this.status_code = status_code;
        this.status_msg = status_msg;
        this.user_id = user_id;
        this.token = token;
    }

    public LoginDTO(Integer user_id, String token) {
        this.user_id = user_id;
        this.token = token;
    }

    public LoginDTO() {
    }

    public Integer getStatus_code() {
        return status_code;
    }

    public void setStatus_code(Integer status_code) {
        this.status_code = status_code;
    }

    public String getStatus_msg() {
        return status_msg;
    }

    public void setStatus_msg(String status_msg) {
        this.status_msg = status_msg;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
