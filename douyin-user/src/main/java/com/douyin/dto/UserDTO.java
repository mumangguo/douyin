package com.douyin.dto;

import com.douyin.entity.Users;

public class UserDTO {
    /**
     * 状态码，0-成功，其他值-失败
     */
    private long statusCode;
    /**
     * 返回状态描述
     */
    private String statusMsg;
    /**
     * 用户信息
     */
    private User user;

    public UserDTO() {
    }

    public UserDTO(long statusCode, String statusMsg, User user) {
        this.statusCode = statusCode;
        this.statusMsg = statusMsg;
        this.user = user;
    }

    public long getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(long statusCode) {
        this.statusCode = statusCode;
    }

    public String getStatusMsg() {
        return statusMsg;
    }

    public void setStatusMsg(String statusMsg) {
        this.statusMsg = statusMsg;
    }

    public User getUser() {
        return user;
    }

    public void setUsers(User user) {
        this.user = user;
    }
}
