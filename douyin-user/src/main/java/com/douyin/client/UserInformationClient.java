package com.douyin.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

@Component
@FeignClient("douyin-social")   //调用的服务
public interface UserInformationClient {

    //调用方法的路径
    @GetMapping("/relation/count/{id}")
    public HashMap<String,Integer> getCount(@PathVariable("id")String id);

}
