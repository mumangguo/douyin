package com.douyin.controller;

import com.douyin.client.UserInformationClient;
import com.douyin.dto.LoginDTO;
import com.douyin.dto.UserDTO;
import com.douyin.service.UserService;
import com.douyin.utils.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;

@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private UserService userService;

    //注入远程调用的接口
    @Autowired
    private UserInformationClient userInformationClient;

    @PostMapping("/login/")
    public LoginDTO login(@RequestParam("username") String username,
                          @RequestParam("password") String password){
        return userService.login(username, password);
    }

    @PostMapping("/register/")
    public LoginDTO register(@RequestParam("username") String username,
                          @RequestParam("password") String password){
        return userService.register(username, password);
    }

    @GetMapping("/")
    public UserDTO getUser(@RequestParam("user_id") String id,
                           @RequestParam("token") String token){
        //先判断id是否为null
        if(id==null){
            return new UserDTO(1,"登录失败",null);
        }

        //调用jwt工具类的方法，根据request对象获取头信息，返回id
        String userId = JwtUtils.getMemberIdByJwtToken(token);

        //如果id为空说明没有登录成功
        if(userId==null){
            return new UserDTO(1,"登录失败",null);
        }

        //根据id查询用户的关注数和粉丝数
        HashMap<String, Integer> map = userInformationClient.getCount(userId);
        int followCount=map.get("followCount");
        int followerCount=map.get("followerCount");
        return userService.getUser(id, token,followCount,followerCount);
    }
}
