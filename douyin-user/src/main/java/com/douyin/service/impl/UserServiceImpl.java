package com.douyin.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douyin.dto.LoginDTO;
import com.douyin.dto.User;
import com.douyin.dto.UserDTO;
import com.douyin.entity.Users;
import com.douyin.mapper.UserMapper;
import com.douyin.service.UserService;
import com.douyin.utils.JwtUtils;
import com.douyin.utils.MD5;
import com.douyin.utils.UserHolder;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, Users> implements UserService {
    @Resource
    private UserMapper userMapper;

    @Override
    public LoginDTO login(String username, String password) {
        //用户名和密码非空判断
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            return new LoginDTO(1, "用户名或密码不能为空", null, null);
        }
        //判断手机号是否正确
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("name",username);
        wrapper.eq("password",MD5.encrypt(password));
        Users users = baseMapper.selectOne(wrapper);

        //判断账号密码
        if (users==null){
            return new LoginDTO(1, "账号或密码错误", null, null);
        }

        //登录成功
        //将id和用户名存入token
        String token = JwtUtils.getJwtToken(users.getId(),users.getName());
        return new LoginDTO(0, "success",  Integer.parseInt(users.getId()+""), token);
    }

    @Override
    public LoginDTO register(String username, String password) {

        //因为前端传过来定义好了只有账号密码，我们给个默认用户名
        //用户名和密码非空判断
        if(StringUtils.isEmpty(username) || StringUtils.isEmpty(password)){
            return new LoginDTO(1, "用户名或密码不能为空", null, null);
        }
        //判断用户名是否重复,表里存着相同用户名不进行添加
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("name",username);
        Integer count = baseMapper.selectCount(wrapper);
        if (count>0){
            return new LoginDTO(1, "改用户名已经存在", null, null);
        }
        //将数据添加到数据库中
        Users user = new Users();
        user.setName(username);
        user.setPassword(MD5.encrypt(password));
        baseMapper.insert(user);

        //获取用户id待会要存token
        QueryWrapper<Users> newWrapper = new QueryWrapper<>();
        newWrapper.eq("name",username);
        user = baseMapper.selectOne(newWrapper);

        //注册成功
        //使用jwt工具类生成token
        String token = JwtUtils.getJwtToken(user.getId(),user.getName());
        return new LoginDTO(0, "success", user.getId(), token);
    }

    @Override
    public UserDTO getUser(String id, String token,int followCount,int followerCount) {

        //把用户信息保存到数据库
        Users users=new Users();
        users.setId(Integer.valueOf(id));
        users.setFollowCount(followCount);
        users.setFollowerCount(followerCount);
        baseMapper.updateById(users);

        //查出Users的信息返回
        QueryWrapper<Users> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        users = baseMapper.selectOne(wrapper);
        users.setPassword(null);

        User user = new User();
        user.setId(users.getId());
        user.setName(users.getName());
        user.setFollow_count(users.getFollowCount());
        user.setFollower_count(users.getFollowerCount());
        user.setIs_follow(users.getIsFollow());
        return new UserDTO(0,"success",user);
    }


}
