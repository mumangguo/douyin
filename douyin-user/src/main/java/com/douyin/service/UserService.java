package com.douyin.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.douyin.dto.LoginDTO;
import com.douyin.dto.UserDTO;
import com.douyin.entity.Users;

public interface UserService extends IService<Users> {
    public LoginDTO login(String username, String password);

    public LoginDTO register(String username, String password);

    UserDTO getUser(String id, String token,int followCount,int followerCount);
}
