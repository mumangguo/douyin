package com.douyin.follow.dto;

import com.douyin.dto.R;

import java.util.List;

/**
 * 关注列表
 */
public class FollowListDTO extends R {
    /**
     * 用户信息列表
     */
    private List<UserDTO> user_list;

    public List<UserDTO> getUser_list() {
        return user_list;
    }

    public void setUser_list(List<UserDTO> user_list) {
        this.user_list = user_list;
    }
}
