package com.douyin.follow.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {
    private static final long serialVersionUID = 569612068214892433L;
    /**
     * 用户id
     */
    private int id;
    /**
     * 用户名
     */
    private String name;

    /**
     * 关注数
     */
    private int followCount;
    /**
     * 粉丝数
     */
    private int followerCount;
    /**
     * 是否已关注
     */
    private boolean isFollow;

    public UserDTO() {
    }

    public UserDTO(int id, String name, int followCount, int followerCount, boolean isFollow) {
        this.id = id;
        this.name = name;
        this.followCount = followCount;
        this.followerCount = followerCount;
        this.isFollow = isFollow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("follow_count")
    public int getFollowCount() {
        return followCount;
    }

    public void setFollowCount(int followCount) {
        this.followCount = followCount;
    }

    @JsonProperty("follower_count")
    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    @JsonProperty("is_follow")
    public boolean isFollow() {
        return isFollow;
    }

    public void setFollow(boolean follow) {
        isFollow = follow;
    }
}
