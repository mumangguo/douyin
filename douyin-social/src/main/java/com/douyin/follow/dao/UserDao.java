package com.douyin.follow.dao;

import com.douyin.follow.dto.UserDTO;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface UserDao {

    /**
     * 根据用户id查询用户信息
     */
    UserDTO queryUserById(String id);
}
