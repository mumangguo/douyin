package com.douyin.follow.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douyin.entity.Follows;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;
@Mapper
@Repository
public interface SocialDao extends BaseMapper<Follows> {

    /**
     * 关注操作
     */
    void action(String user_id, String follower_id, String cancel);

    /**
     * 取消关注操作
     */
    void cancelAction(String user_id, String follower_id, String cancel);

    /**
     * 关注列表
     */
    List<Follows> followList(String user_id);

    /**
     * 粉丝列表
     */
    List<Follows> followerList(String user_id);

    /**
     * 根据用户id查询关注用户的总数
     */
    int followCount(String user_id);


    /**
     * 根据用户id查询粉丝总数
     */
    int followerCount(String user_id);

    /**
     * 根据用户id和关注者id查询是否关注
     */
    Object isFollow(String user_id, String follower_id);
}
