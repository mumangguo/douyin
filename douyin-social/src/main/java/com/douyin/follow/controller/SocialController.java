package com.douyin.follow.controller;

import com.douyin.follow.dto.FollowListDTO;
import com.douyin.dto.R;
import com.douyin.follow.dto.UserDTO;
import com.douyin.follow.service.SocialService;
import com.douyin.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/relation")
public class SocialController {

    private static final Logger logger = LoggerFactory.getLogger(SocialController.class);
    @Resource
    private SocialService socialService;

    @Autowired
    public SocialController(SocialService socialService) {
        this.socialService = socialService;
    }

    /**
     * 关注与取消关注
     */
    @PostMapping("/action/")
    public R action(@RequestParam("token") String token,
                    @RequestParam("to_user_id") String follower_id,
                    @RequestParam("action_type") String cancel) {
        R r = new R();
        //关注操作
        if (cancel.equals("1")) {
            //判断是否是关注的是自己
            if (JwtUtils.getMemberIdByJwtToken(token).equals(follower_id)) {
                r.setStatus_code(1);
                r.setStatus_msg("宝，不能关注自己哦！");
                return r;
            }
            try {
                socialService.action(token, follower_id, cancel);
                r.setStatus_code(0);
                r.setStatus_msg("success");
                return r;
            } catch (Exception e) {
                e.printStackTrace();
                r.setStatus_code(1);
                r.setStatus_msg("宝，该用户已关注！"+e.getMessage());
                return r;
            }
        }
        //取关操作
        try {
            socialService.cancelAction(token, follower_id, cancel);
            r.setStatus_code(0);
            r.setStatus_msg("success");
            return r;
        } catch (Exception e) {
            r.setStatus_code(1);
            r.setStatus_msg(e.getMessage());
            return r;
        }
    }

    /**
     * 关注列表
     */
    @GetMapping("/follow/list/")
    public FollowListDTO followList(@RequestParam("token") String token,
                                    @RequestParam("user_id") String user_id) {
        FollowListDTO followListDTO = new FollowListDTO();
        try {
            List<UserDTO> users = socialService.followList(user_id);
            followListDTO.setStatus_code(0);
            followListDTO.setStatus_msg("success");
            followListDTO.setUser_list(users);
            return followListDTO;
        } catch (Exception e) {
            followListDTO.setStatus_code(1);
            followListDTO.setStatus_msg(e.getMessage());
            return followListDTO;
        }
    }

    /**
     * 粉丝列表
     */
    @GetMapping("/follower/list/")
    public FollowListDTO followerList(@RequestParam("token") String token,
                                      @RequestParam("user_id") String user_id) {
        FollowListDTO followListDTO = new FollowListDTO();
        try {
            List<UserDTO> users = socialService.followerList(user_id);
            followListDTO.setStatus_code(0);
            followListDTO.setStatus_msg("success");
            followListDTO.setUser_list(users);
            return followListDTO;
        } catch (Exception e) {
            followListDTO.setStatus_code(1);
            followListDTO.setStatus_msg(e.getMessage());
            return followListDTO;
        }
    }

    /**
     * 根据用户id查询关注数和粉丝数
     */
    @GetMapping("/count/{id}")
    public HashMap<String,Integer> getCount(@PathVariable("id")String id){
        return socialService.getCount(id);
    }
}
