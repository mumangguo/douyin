package com.douyin.follow.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douyin.entity.Follows;
import com.douyin.follow.dto.UserDTO;

import java.util.HashMap;
import java.util.List;

public interface SocialService extends IService<Follows> {

    /**
     * 关注操作
     */
    void action(String token, String follower_id, String cancel);

    /**
     * 取消关注操作
     */
    void cancelAction(String token, String follower_id, String cancel);

    /**
     * 关注列表
     */
    List<UserDTO> followList(String user_id);

    /**
     * 粉丝列表
     */
    List<UserDTO> followerList( String user_id);

    HashMap<String,Integer> getCount(String id);
}
