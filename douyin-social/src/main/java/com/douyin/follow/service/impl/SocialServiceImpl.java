package com.douyin.follow.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douyin.entity.Follows;
import com.douyin.follow.dao.SocialDao;
import com.douyin.follow.dao.UserDao;
import com.douyin.follow.dto.UserDTO;
import com.douyin.follow.service.SocialService;
import com.douyin.utils.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
@Transactional
public class SocialServiceImpl extends ServiceImpl<SocialDao, Follows> implements SocialService {

    private static final Logger logger = LoggerFactory.getLogger(SocialServiceImpl.class);
    @Resource
    private SocialDao socialDao;

    @Resource
    private UserDao userDao;

    /**
     * 关注操作 后续优化 现在不通过1和2判断是否关注
     */
    @Override
    public void action(String token, String follower_id, String cancel) {
        socialDao.action(JwtUtils.getMemberIdByJwtToken(token), follower_id, cancel);
    }

    /**
     * 取消关注操作
     */
    @Override
    public void cancelAction(String token, String follower_id, String cancel) {
        socialDao.cancelAction(JwtUtils.getMemberIdByJwtToken(token), follower_id, cancel);
    }

    /**
     * 获取关注列表
     */
    @Override
    public List<UserDTO> followList(String user_id) {
        //根据用户id查询关注列表
        List<Follows> followsList = socialDao.followList(user_id);
        List<UserDTO> usersList = new ArrayList<>();
        //根据关注列表中的用户id查询用户信息
        for (Follows follows : followsList) {
            System.err.println(follows);
            System.err.println(followsList);
            UserDTO userDTO = userDao.queryUserById(String.valueOf(follows.getFollower_id()));
            if (userDTO != null) {
                userDTO.setFollowCount(socialDao.followCount(String.valueOf(follows.getFollower_id())));
                userDTO.setFollowerCount(socialDao.followerCount(String.valueOf(follows.getFollower_id())));
                Object isFollow = socialDao.isFollow(user_id, String.valueOf(follows.getFollower_id()));
                if (isFollow == null) {
                    userDTO.setFollow(false);
                } else {
                    userDTO.setFollow(true);
                }
                usersList.add(userDTO);
            }
        }
        return usersList;
    }

    /**
     * 粉丝列表
     */
    @Override
    public List<UserDTO> followerList(String user_id) {
        //根据用户id查询粉丝列表
        List<Follows> followsList = socialDao.followerList(user_id);
        List<UserDTO> usersList = new ArrayList<>();
        //根据粉丝列表中的用户id查询用户信息
        followsList.forEach(follow -> {
            UserDTO userDTO = userDao.queryUserById(String.valueOf(follow.getUser_id()));
            if (userDTO != null) {
                userDTO.setFollowCount(socialDao.followCount(String.valueOf(follow.getUser_id())));
                userDTO.setFollowerCount(socialDao.followerCount(String.valueOf(follow.getUser_id())));
                Object isFollow = socialDao.isFollow(user_id, String.valueOf(follow.getUser_id()));
                if (isFollow == null) {
                    userDTO.setFollow(false);
                } else {
                    userDTO.setFollow(true);
                }
                usersList.add(userDTO);
            }
        });
        return usersList;
    }

    /**
     * 根据id返回关注数和粉丝数
     */
    @Override
    public HashMap<String, Integer> getCount(String id) {
        QueryWrapper<Follows> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", id);
        int followCount = baseMapper.selectCount(queryWrapper);
        int followerCount = baseMapper.followerCount(id);
        System.out.println("用户" + id + "的关注数为" + followCount + ",粉丝数为" + followerCount);
        HashMap<String, Integer> map = new HashMap<>();
        map.put("followCount", followCount);
        map.put("followerCount", followerCount);
        return map;
    }
}
