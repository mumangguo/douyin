package com.douyin.message.controller;

import com.douyin.dto.R;
import com.douyin.message.dto.MessageDTO;
import com.douyin.message.service.MessageService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 20:59
 */

@RestController
@RequestMapping("/douyin/message")
public class MessageController {
    @Resource
    private MessageService messageService;

    //发送消息
    @PostMapping("/action")
    public R action(@RequestParam("token") String token,
                    @RequestParam("to_user_id") String toUserId,
                    @RequestParam("action_type") String actionType,
                    @RequestParam("content") String content){
        return messageService.action(token,toUserId,actionType,content);
    }

    //聊天记录
    @GetMapping("/chat")
    public MessageDTO chat(@RequestParam("token") String token,
                           @RequestParam("to_user_id") String toUserId){
        return messageService.chat(token,toUserId);
    }
}
