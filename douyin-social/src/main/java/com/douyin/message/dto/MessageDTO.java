package com.douyin.message.dto;

import com.douyin.dto.R;
import com.douyin.entity.Message;

import java.io.Serializable;
import java.util.List;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 22:17
 */
public class MessageDTO extends R{

    private List<Message> message_list; //消息记录

    public List<Message> getMessage_list() {
        return message_list;
    }

    public void setMessage_list(List<Message> message_list) {
        this.message_list = message_list;
    }
}
