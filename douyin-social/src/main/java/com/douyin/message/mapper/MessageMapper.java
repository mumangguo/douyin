package com.douyin.message.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.douyin.entity.Message;
import org.apache.ibatis.annotations.Mapper;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 21:04
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {
}
