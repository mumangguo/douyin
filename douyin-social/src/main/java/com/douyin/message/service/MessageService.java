package com.douyin.message.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.douyin.dto.R;
import com.douyin.entity.Message;
import com.douyin.message.dto.MessageDTO;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 21:03
 */
public interface MessageService extends IService<Message> {
    R action(String token, String toUserId, String actionType, String content);

    MessageDTO chat(String token, String toUserId);
}
