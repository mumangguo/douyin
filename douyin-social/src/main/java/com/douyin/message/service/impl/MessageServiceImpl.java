package com.douyin.message.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.douyin.dto.R;
import com.douyin.entity.Message;
import com.douyin.message.dto.MessageDTO;
import com.douyin.message.mapper.MessageMapper;
import com.douyin.message.service.MessageService;
import com.douyin.utils.JwtUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 21:03
 */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService {
    @Resource
    private MessageMapper messageMapper;
    //发送消息
    @Override
    public R action(String token, String toUserId, String actionType, String content) {
        if (actionType.equals("1")){
            Message message = new Message();
            String userId = JwtUtils.getMemberIdByJwtToken(token);
            message.setUserId(Integer.parseInt(userId));
            message.setToUserId(Integer.parseInt(toUserId));
            message.setContent(content);
            message.setCreateTime(LocalDateTime.now().toString());
            boolean save = save(message);
            if (save) {
                return R.success("发送成功");
            }
        }
        return R.fail("发送失败");
    }

    //聊天记录
    @Override
    public MessageDTO chat(String token, String toUserId) {
        MessageDTO messageDTO = new MessageDTO();
        String userId = JwtUtils.getMemberIdByJwtToken(token);
        LambdaQueryWrapper<Message> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Message::getUserId,userId);
        queryWrapper.eq(Message::getToUserId,toUserId);
        List<Message> messages = messageMapper.selectList(queryWrapper);
        if (messages.isEmpty()){
            messageDTO.setStatus_code(-1);
            messageDTO.setStatus_msg("暂无聊天记录");
            return messageDTO;
        }
        messageDTO.setMessage_list(messages);
        messageDTO.setStatus_code(0);
        messageDTO.setStatus_msg("1");
        return messageDTO;
    }
}
