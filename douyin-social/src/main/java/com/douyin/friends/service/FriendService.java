package com.douyin.friends.service;

import com.douyin.follow.dto.FollowListDTO;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 20:55
 */
public interface FriendService {
    FollowListDTO getFriends(String userId);
}
