package com.douyin.friends.service.impl;

import com.douyin.follow.dto.FollowListDTO;
import com.douyin.follow.dto.UserDTO;
import com.douyin.follow.service.SocialService;
import com.douyin.friends.service.FriendService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 20:55
 */
@Service
public class FriendServiceImpl implements FriendService {
    @Resource
    private SocialService socialService;
    @Override
    public FollowListDTO getFriends(String userId) {
        FollowListDTO followListDTO = new FollowListDTO();
        //查询出该用户的所有关注
        List<UserDTO> userDTOS = socialService.followList(userId);
        List<UserDTO> friendList = new ArrayList<>();
        for (UserDTO userDTO : userDTOS) {
            String followId = userDTO.getId()+"";
            //查询出该用户关注的所有关注
            List<UserDTO> follows = socialService.followList(followId);
            System.out.println("该用户所有关注：{"+follows+"}");
            for (UserDTO follow : follows) {
                int id = Integer.parseInt(userId);
                if (id==follow.getId()){
                    //如果关注中有该用户则视为好友
                    friendList.add(follow);
                }
            }
        }
        if (friendList==null){
            followListDTO.setStatus_code(-1);
            followListDTO.setStatus_msg("查询失败");
            followListDTO.setUser_list(null);
            return followListDTO;
        }
        followListDTO.setStatus_code(0);
        followListDTO.setStatus_msg("success");
        followListDTO.setUser_list(friendList);
        return followListDTO;
    }
}
