package com.douyin.friends.controller;

import com.douyin.follow.dto.FollowListDTO;
import com.douyin.follow.dto.UserDTO;
import com.douyin.follow.service.SocialService;
import com.douyin.friends.service.FriendService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @version: java version 1.8
 * @Author: Mr Orange
 * @description:
 * @date: 2023-01-14 20:09
 */
@RestController
@RequestMapping("/douyin/relation/friend/")
public class friendsController {
    @Resource
    private FriendService friendService;

    @GetMapping("list")
    public FollowListDTO friendList(@RequestParam("user_id") String userId,
                                    @RequestParam("token") String token){
        return friendService.getFriends(userId);
    }
}
